🎯 <b>Projeto fictício de Redes - Hospital Ônix</b>

Este é um projeto de redes fictício, que tem como objetivo aplicar na prática o conhecimento acadêmico adquirido, em um ambiente mais próximo possível do real. Ganhando assim experiência e com isso agregar valor ao portfólio.

A infraestrutura de TI nos hospitais, engloba equipamentos específicos de medicina, computadores, impressoras, roteadores, switches, servidores e diversos outros dispositivos de rede. Em última análise, estes são usados para: monitorar e auxiliar a execução de procedimentos clínicos e médicos, registrar e acessar os dados de todos os pacientes e médicos, marcar e monitorar consultas e exames e outras atividades de gerenciamento. Portanto, ter uma boa infraestrutura de rede não é somente importante, mas também primordial para as atividades essenciais de um hospital.

Sabendo disso, estamos apresentando a seguir uma proposta de projeto de rede (projeto Lógico, projeto Físico e modelo de Simulação no Packet Tracer) para as três unidades do hospital Ônix de Curitiba, que objetiva melhorar a qualidade dos serviços, agilizar as tomadas de decisão, melhorar a comunicação corporativa, a segurança e confiabilidade de aplicações e dados de missão crítica.<br><br>

⭕ <b>Levantamento de Requisitos</b>

<b>Unidades escolhidas</b>	
<table>
        <tr>
                <td width="310"><img src="https://gitlab.com/clazgr/rede-hospitalar-onix/-/raw/master/hospital-onix-mateus-leme.jpg"></td>
                <td>
                        <B>HOSPITAL ÔNIX MATEUS LEME:</b><br>
                        Estrutura: 2 andares<br>
                        - 1 Sala de Emergência<br>
                        - 40 leitos<br>
                        - Centro Cirúrgico com 6 amplas salas.<br>
                        - 3 Laboratórios<br>
                        - 6 Consultórios<br>
                        - 10 leitos de UTI<br>
                        - 2 Salas Administrativo<br>
                        - 2 Recepção e Sala de Espera<br>
                        - Guarita / Estacionamento<br>
                        - Número de Hosts: 117<br>
                        Rua Mateus Leme, 2600 São Francisco, Curitiba - PR<br>
                        Telefone: (41) 3301-6000<br>
                        http://www.hospitalonix.com.br
                </td>
        </tr>
        <tr>
                <td width="310"><img src="https://gitlab.com/clazgr/rede-hospitalar-onix/-/raw/master/hospital-onix-batel.jpg"></td>
                <td>
                        <b>HOSPITAL ÔNIX BATEL:</b><br>
                        Estrutura: 3 andares<br>
                        - 1 Sala de Emergência<br>
                        - 40 leitos<br>
                        - Centro Cirúrgico com 6 amplas salas<br>
                        - 3 Laboratórios<br>
                        - 6 Consultórios<br>
                        - 10 leitos de UTI<br>
                        - 2 Salas Administrativo<br>
                        - 2 Recepção e Sala de Espera<br>
                        - Guarita / Estacionamento<br>
                        - Número de Hosts: 118<br>
                        Av. Vicente Machado, 2321 Batel, Curitiba - PR<br>
                        Telefone: (41) 3017-1200<br>
                        http://www.hospitalonix.com.br
                </td>
        </tr>
        <tr>
                <td width="310"><img src="https://gitlab.com/clazgr/rede-hospitalar-onix/-/raw/master/hospital-onix-PAMI.jpg"></td>
                <td>
                        <b>HOSPITAL ÔNIX – PAMI (PRONTO ATENDIMENTO MÉDICO INFANTIL) 24H</b><br>
                        Estrutura: 1 andar<br>
                        - 20 leitos<br>
                        - Centro Cirúrgico com 2 amplas salas<br>
                        - 1 Laboratório<br>
                        - 3 Consultórios<br>
                        - 5 leitos de UTI<br>
                        - 1 Salas Administrativo<br>
                        - 1 Recepção e Sala de Espera<br>
                        - Número de Hosts: 63<br>
                        Rua Vicente Machado, 2340 Batel, Curitiba - PR<br>
                        Telefone: (41) 3021-3001<br>
                        https://www.clinipam.com.br/centros-clinicos/clinipam-pami
                </td>
        </tr>
</table><br>
<b>Mapa com as localidades - Visão geral e visão aproximada</b><br><br>
<img src="https://gitlab.com/clazgr/rede-hospitalar-onix/-/raw/master/panorama_geral.png"><br><br>
<b>Mapa com as localidades - Visão geral e distâncias</b><br><br>
<b>Distância:</b> Unidade Mateus Leme para Unidade PAMI: 6.2 Km<br>
<img src="https://gitlab.com/clazgr/rede-hospitalar-onix/-/raw/master/panorama_geral_distancia-batel-mateusleme.png"><br><br>
<b>Distância:</b> Unidade PAMI para Unidade Batel: 170m<br>
<img src="https://gitlab.com/clazgr/rede-hospitalar-onix/-/raw/master/panorama_geral_distancia-batel-pami.png"><br><br><br>


⭕ <b>Projeto Físico de Rede</b>

<b>Diagrama distribuição física (planta baixa)</b>

Devido a dificuldade para conseguir junto ao hospital as plantas baixas reais das unidades, foi necessário recorrer a pesquisa de internet para obter as plantas que aqui serão apresentadas. Estas plantas foram retiradas do site da Arquiteta e Urbanista, Deborah de Brito. Tendo o único objetivo de representar de forma fictícia as unidades do hospital Ônix. Representação esta, apenas para fins de estudo acadêmico.<br>

A planta original pode ser encontrada no site:<br>
Link: https://deborahdebrito.blogspot.com/p/projetos-de-arquitetura-hospitalar.html<br><br> 

<b>Planta da Unidade PAMI</b><br>
<img src="https://gitlab.com/clazgr/rede-hospitalar-onix/-/raw/master/planta_baixa_hospital-pami.png"><br><br>
<b>Planta das Unidades Batel e Mateus Leme</b><br>
<img src="https://gitlab.com/clazgr/rede-hospitalar-onix/-/raw/master/planta_baixa_hospital-batel-mateusleme_NEW.PROJECT.png"><br>

<b>Diagrama Físico de Rede</b>

Este diagrama de rede tem a finalidade de apresentar em uma visão macro, a topologia física da rede das três unidades do hospital Ônix. Apresentando de forma clara e sucinta, as conexões de Wan e Lan, tecnologias e serviços a serem utilizados, bem como, os hosts e ativos de rede envolvidos nesse projeto.

<b>Tecnologias e Serviços utilizados para conexão</b>

Este projeto utilizará para interconectar as unidades o serviço de MPLS, que é uma tecnologia de encaminhamento de pacotes que usa rótulos para tomar decisões sobre o encaminhamento de dados. A principal aplicação desta tecnologia será prover VPN, tendo como principais benefícios: A facilidade de gerencia, desempenho com garantia de qualidade de serviço(QOS), disponibilidade, segurança e escalabilidade.
Devido ao custo do MPLS, resolvemos utilizar ele apenas para interconexão entre as unidades, deixando a conexão de internet a cargo de uma banda larga corporativa (link não dedicado).
Para o serviço de voz, utilizaremos o serviço de telefonia SIP com link dedicado e o serviço de pabx virtual, que combina telefonia , qualidade de rede e a flexibilidade de serviços de voz para ampliar o leque das facilidades dos serviços de telefonia. Por ser virtual, sua instalação e manutenção são mais simples e rápidas do que a de um pabx tradicional, além de permitir atualizações constantes de tecnologia e serviços, o que acaba reduzindo custos.<br>

<b>Visão Geral do diagrama físico de rede</b>

<img src="https://gitlab.com/clazgr/rede-hospitalar-onix/-/raw/master/Diagrama_PROJETO-ONIX-A4_NEW.PROJECT.png">

<b>Número de Pontos de Rede e Local da Instalação - Unidades Batel e Mateus Leme</b><br>
<img src="https://gitlab.com/clazgr/rede-hospitalar-onix/-/raw/master/Distribui%C3%A7%C3%A3o_Pontos_de_Rede___Unidade_Batel-Mateus_Leme-PAMI_NEW.PROJECT.jpg"><br><br>

<b>Número de Pontos de Rede e Local da Instalação - Unidade PAMI</b><br>
<img src="https://gitlab.com/clazgr/rede-hospitalar-onix/-/raw/master/Distribui%C3%A7%C3%A3o_Pontos_de_Rede___Unidade_PAMI.jpg"><br><br>

<b>Relação dos Dispositivos Finais e Intermediários de Rede</b><br><br>
<img src="https://gitlab.com/clazgr/rede-hospitalar-onix/-/raw/master/Rela%C3%A7%C3%A3o_Dispositivos_de_Rede_NEW.PROJECT.jpg"><br>

<b>OBS.:</b> Os Roteadores das unidades serão fornecidos pela operadora em regime de comodato. Sendo assim, toda manutenção e suporte será dado por ela. As configurações são feitas pela operadora obedecendo as necessidades do cliente.<br><br>

<b>Sobre os dispositivos escolhidos</b><br><br>
<b>Switch Cisco SG350X</b><br>
Este modelo específico de switch foi escolhido devido alguns recursos que ele possui que serão indispensáveis para o funcionamento da rede. São eles:
1. StackWise: Nos locais onde haverá mais de um switch por andar, estes serão interligados através da feature de StackWise. Recurso este que permite que vários switches sejam “empilhados” e atuem como um só dispositivo, facilitando o gerenciamento das switches.
2. PoE: O Power over Ethernet é uma funcionalidade de rede definida pelas normas IEEE 802.3af e 802.3at, que permite que os cabos de Ethernet forneçam energia a dispositivos de rede através da ligação de dados existente. Os access point e as câmeras IP utilizarão esse recurso.
Além disso, esse modelo possui 4 portas uplink de 10GE no switch de 48 portas e pode operar também na camada 3. Suportando: ACL, QoS, proteção avançada contra ameaças, segurança de primeiro salto IPv6, roteamento estático, LACP, RSTP, etc. <br>

<b>Cisco Access Point Aironet 1815m</b><br>
Este AP oferece suporte a uma área de cobertura maior exigindo assim menos pontos de acesso. Ele possui suporte para o padrão Wi-Fi 802.11ac Wave 2 do IEEE, com número máximo de 200 clientes sem fio associados por banda de rádio Wi-Fi. Totalizando 400 clientes por ponto de acesso. Suporta Power over Ethernet (PoE) 802.3af, configuração de VLAN dinâmica ou por porta, e muitos outros recursos que fazem deste equipamento a escolha ideal para o projeto. <br>

<b>NVR Intelbras iNVD 5032NVR</b><br>
NVR (Network Video Recorder) é um equipamento que grava, arquiva e gerencia as imagens obtidas por câmeras IP. Este equipamento da Intelbras possui boas especificações, com ótimo custo-benefício. Vale destacar: 12TB de armazenamento, throughput de rede de até 200 Mbs, compatível com tecnologia H.265e H.265+, resolução até 12 MP, reconhecimento facial em tempo real, suportando até 20.000 faces cadastradas. <br><br>


⭕ <b>Projeto Lógico de Rede</b>

<b>Protocolo de Rede</b>

Para este projeto, foram escolhidos os protocolos de rede IPv4 e IPv6, que trabalharão em Dual Stack.
A escolha de se trabalhar com os dois protocolos se deu por alguns fatores:
1. O protocolo IPv4 ainda está na ativa e assim ficará por um bom período até que não seja mais utilizado.
2. O protocolo IPv6 ainda não está em uso em todos os dispositivos de rede, portanto, utilizar somente este protocolo traria problemas de acesso.
3. Como sabemos, o estoque de endereços IPv4 para a região da América Latina e o Caribe esgotou-se na data 19/8/2020. Por isso, o uso do IPv6 além de importante é necessário. Segundo o APNIC, no Brasil a compatibilidade com o IPv6 é 36,70% e nos Estados Unidos já é de 51,94%, portanto, uma realidade.<br>

Por estes motivos, e pensando na transição entre os protocolos, este projeto utilizará o método dual stack.<br><br>
Vale ressaltar que não há comunicação entre o protocolo IPv6 e IPv4, ou seja, a camada de transporte escolhe enviar seu fluxo por um ou por outro de acordo com o que a aplicação for utilizar. Dessa maneira, quando o host for se comunicar com outros hosts IPv4 ele utiliza a pilha do protocolo IP versão 4, porém quando for conversar com um host ou servidor IPv6 utilizará a pilha referente ao protocolo IP versão 6.<br><br>

<b>Esquema de endereçamento IPv4 / IPv6 e Vlans<br>

<br>
<img src="https://gitlab.com/clazgr/rede-hospitalar-onix/-/raw/master/Plano_de_endere%C3%A7amento_IPv4-IPv6_e_Vlan-BATEL.jpg">

<br><br>
<img src="https://gitlab.com/clazgr/rede-hospitalar-onix/-/raw/master/Plano_de_endere%C3%A7amento_IPv4-IPv6_e_Vlan-PAMI.jpg">

<br><br>
<img src="https://gitlab.com/clazgr/rede-hospitalar-onix/-/raw/master/Plano_de_endere%C3%A7amento_IPv4-IPv6_e_Vlan-ML.jpg"><br>
<br>

<img src="https://gitlab.com/clazgr/rede-hospitalar-onix/-/raw/master/Plano_de_distribui%C3%A7%C3%A3o_de_endere%C3%A7os_IPv4-IPv6_fixo_e_Vlan-BATEL.jpg"><br>
<img src="https://gitlab.com/clazgr/rede-hospitalar-onix/-/raw/master/Plano_de_distribui%C3%A7%C3%A3o_de_endere%C3%A7os_IPv4-IPv6_fixo_e_Vlan-PAMI.jpg"><br>
<img src="https://gitlab.com/clazgr/rede-hospitalar-onix/-/raw/master/Plano_de_distribui%C3%A7%C3%A3o_de_endere%C3%A7os_IPv4-IPv6_fixo_e_Vlan-ML.jpg"><br>

<b>Pontos Importantes do esquema de endereçamento IPv4 / IPv6</b></br>

O número de hosts dos APs (Convidado e Corporativo) é o número máximo de conexões simultâneas que poderá haver e não que necessariamente haverá.
Para o cálculo de sub-rede da Vlan de Voz_N1, foi utilizado o mesmo cálculo de N2 ou seja, foi considerado o número de host de N2. Pois devido a política da empresa, pode acontecer de um host N1 se transformar em N2 e vice e versa.
Algumas sub-redes como “Adm” por exemplo, poderia ter sido feito um CIDR maior. Ou seja, ao invés de um /28 (14 utilizáveis) para alocar os 9 IPs da rede, foi optado por um /27 (30 utilizáveis) para que houvesse uma reserva maior de IPs. Assim prevendo uma possível expansão.

<b>Mapeamento das Vlans nas Switches</b>

O esquemático que será apresentado a seguir documenta de forma clara as conexões das interfaces das switches. Ele registra o mapeamento das vlans, conexões de acesso (hosts) e de tronco (entre switches e roteadores), etherchannel e portas disponíveis.
Por se tratar de estruturas idênticas, as unidades Batel e Mateus Leme estão retratadas em um único mapeamento.<br>

<img src="https://gitlab.com/clazgr/rede-hospitalar-onix/-/raw/master/Mapeamento_das_Vlans_no_switch-NEW.PROJECT-ML-BATEL-TI1.jpg">
<img src="https://gitlab.com/clazgr/rede-hospitalar-onix/-/raw/master/Mapeamento_das_Vlans_no_switch-NEW.PROJECT-ML-BATEL-TI2.jpg">
<br><br>

<img src="https://gitlab.com/clazgr/rede-hospitalar-onix/-/raw/master/Mapeamento_das_Vlans_no_switch-NEW.PROJECT-PAMI.jpg"><br><br>

<b>Diagrama Lógico de Rede</b><br><br>

<img src="https://gitlab.com/clazgr/rede-hospitalar-onix/-/raw/master/topologia-logica.png">

⭕ <b>Modelo de Simulação Packet Tracer</b>

O Packet Tracer é um programa educacional que permite a simulação, visualização, criação e avaliação de uma rede de computadores, através de equipamentos e configurações presentes em situações reais. 
Devido a todas essas características, estaremos implantando o projeto neste ambiente de simulação antes da implantação no ambiente real. Assim poderemos ter uma boa base de como tudo funcionará. <br><br>

<b>Configurações em andamento</b><br>
Para facilitar o acompanhamento do projeto, segue abaixo as configurações dos dispositivos intermediários de rede conforme o avanço das etapas.

<b>Unidade Batel</b><br>
Configuração RTR1: https://gitlab.com/clazgr/rede-hospitalar-onix/-/blob/master/RTR1-BATEL<br>
Configuração RTR2: https://gitlab.com/clazgr/rede-hospitalar-onix/-/blob/master/RTR2-BATEL<br>
Configuração SW1.MED: https://gitlab.com/clazgr/rede-hospitalar-onix/-/blob/master/SW1.MED.BATEL<br>
Configuração SW1.ADM: https://gitlab.com/clazgr/rede-hospitalar-onix/-/blob/master/SW1.ADM.BATEL<br>
Configuração SW1.TI: https://gitlab.com/clazgr/rede-hospitalar-onix/-/blob/master/SW1.TI.BATEL<br>
Configuração SW2.MED: https://gitlab.com/clazgr/rede-hospitalar-onix/-/blob/master/SW2.MED.BATEL<br>
Configuração SW2.ADM: https://gitlab.com/clazgr/rede-hospitalar-onix/-/blob/master/SW2.ADM.BATEL<br>
Configuração SW2.TI: https://gitlab.com/clazgr/rede-hospitalar-onix/-/blob/master/SW2.TI.BATEL<br>

<b>Unidade PAMI</b><br>
Configuração RTR1: https://gitlab.com/clazgr/rede-hospitalar-onix/-/blob/master/RTR1.PAMI<br>
Configuração RTR2: https://gitlab.com/clazgr/rede-hospitalar-onix/-/blob/master/RTR2.PAMI<br>
Configuração SW1.MED: https://gitlab.com/clazgr/rede-hospitalar-onix/-/blob/master/SW1.MED.PAMI<br>
Configuração SW1.ADM: https://gitlab.com/clazgr/rede-hospitalar-onix/-/blob/master/SW1.ADM.PAMI<br>
Configuração SW1.TI: https://gitlab.com/clazgr/rede-hospitalar-onix/-/blob/master/SW1.TI.PAMI<br>

<b>Unidade Mateus Leme</b><br>
Configuração RTR1: https://gitlab.com/clazgr/rede-hospitalar-onix/-/blob/master/RTR1.ML<br>
Configuração RTR2: https://gitlab.com/clazgr/rede-hospitalar-onix/-/blob/master/RTR2.ML<br>
Configuração SW1.MED: https://gitlab.com/clazgr/rede-hospitalar-onix/-/blob/master/SW1.MED.ML<br>
Configuração SW1.ADM: https://gitlab.com/clazgr/rede-hospitalar-onix/-/blob/master/SW1.ADM.ML<br>
Configuração SW1.TI: https://gitlab.com/clazgr/rede-hospitalar-onix/-/blob/master/SW1.TI.ML<br>
Configuração SW2.MED: https://gitlab.com/clazgr/rede-hospitalar-onix/-/blob/master/SW2.MED.ML<br>
Configuração SW2.ADM: https://gitlab.com/clazgr/rede-hospitalar-onix/-/blob/master/SW2.ADM.ML<br>
Configuração SW2.TI: https://gitlab.com/clazgr/rede-hospitalar-onix/-/blob/master/SW2.TI.ML<br>

<b>ISP</b><br>
Configuração RTR.OPERADORA: https://gitlab.com/clazgr/rede-hospitalar-onix/-/blob/master/RTR-OPERADORA<br><br>
<b>...</b><br><br>
🎯 <b>Arquivo Packet Tracer:</b> https://gitlab.com/clazgr/rede-hospitalar-onix/-/blob/master/ONIX.NEW.PROJECT_VFINAL.pkt<br>

🎯 <b>Documentação Completa do Projeto:</b> https://gitlab.com/clazgr/rede-hospitalar-onix/-/blob/master/projeto.onix.documenta%C3%A7%C3%A3o.completa.pdf<br>
