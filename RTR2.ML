RTR2-ONIX-ML#show startup-config 
Using 5698 bytes
!
version 15.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
service password-encryption
!
hostname RTR2-ONIX-ML
!
!
!
enable secret 5 $1$mERr$FgeFZdgS1iL/FNd8aYVM5/
!
!
!
!
!
!
no ip cef
ipv6 unicast-routing
!
no ipv6 cef
!
ipv6 dhcp pool STATEFUL-VLAN10
 address prefix 2001:DB8:3C4D:310::/64 lifetime 172800 86400
 dns-server 2001:4860:4860::8888
!
ipv6 dhcp pool STATEFUL-VLAN11
 address prefix 2001:DB8:3C4D:311::/64 lifetime 172800 86400
 dns-server 2001:4860:4860::8888
!
ipv6 dhcp pool STATEFUL-VLAN20
 address prefix 2001:DB8:3C4D:320::/64 lifetime 172800 86400
 dns-server 2001:4860:4860::8888
!
ipv6 dhcp pool STATEFUL-VLAN30
 address prefix 2001:DB8:3C4D:330::/64 lifetime 172800 86400
 dns-server 2001:4860:4860::8888
!
ipv6 dhcp pool STATEFUL-VLAN40
 address prefix 2001:DB8:3C4D:340::/64 lifetime 172800 86400
 dns-server 2001:4860:4860::8888
!
ipv6 dhcp pool STATEFUL-VLAN50
 address prefix 2001:DB8:3C4D:350::/64 lifetime 172800 86400
 dns-server 2001:4860:4860::8888
!
ipv6 dhcp pool STATEFUL-VLAN51
 address prefix 2001:DB8:3C4D:351::/64 lifetime 172800 86400
 dns-server 2001:4860:4860::8888
!
!
!
username onix password 7 08354546060A151E060A000B24223C
!
!
license udi pid CISCO2911/K9 sn FTX152453W1-
!
!
!
!
!
!
!
!
!
ip ssh version 2
ip ssh authentication-retries 2
ip ssh time-out 60
no ip domain-lookup
ip domain-name onix.com
!
!
spanning-tree mode pvst
!
!
!
!
!
!
interface GigabitEthernet0/0
 description TRUNK COM SW1.MED
 no ip address
 duplex auto
 speed auto
 standby version 2
!
interface GigabitEthernet0/0.20
 description MEDICAL
 encapsulation dot1Q 20
 ip address 172.18.1.3 255.255.255.128
 ip helper-address 172.18.2.68
 ipv6 address FE80::20 link-local
 ipv6 address 2001:DB8:3C4D:320::2/64
 ipv6 nd managed-config-flag
 ipv6 dhcp server STATEFUL-VLAN20
 standby 20 ip 172.18.1.1
!
interface GigabitEthernet0/1
 description TRUNK COM SW1.TI
 no ip address
 duplex auto
 speed auto
!
interface GigabitEthernet0/1.10
 description VOZ_N1
 encapsulation dot1Q 10
 ip address 172.18.1.131 255.255.255.192
 ip helper-address 172.18.2.68
 ipv6 address FE80::10 link-local
 ipv6 address 2001:DB8:3C4D:310::2/64
 ipv6 nd managed-config-flag
 ipv6 dhcp server STATEFUL-VLAN10
 standby 10 ip 172.18.1.129
!
interface GigabitEthernet0/1.11
 description VOZ_N2
 encapsulation dot1Q 11
 ip address 172.18.1.195 255.255.255.192
 ip helper-address 172.18.2.68
 ipv6 address FE80::11 link-local
 ipv6 address 2001:DB8:3C4D:311::2/64
 ipv6 nd managed-config-flag
 ipv6 dhcp server STATEFUL-VLAN11
 standby 11 ip 172.18.1.193
!
interface GigabitEthernet0/1.40
 description TI
 encapsulation dot1Q 40
 ip address 172.18.2.67 255.255.255.192
 ip helper-address 172.18.2.68
 ipv6 address FE80::40 link-local
 ipv6 address 2001:DB8:3C4D:340::2/64
 ipv6 nd managed-config-flag
 ipv6 dhcp server STATEFUL-VLAN40
 standby 40 ip 172.18.2.65
!
interface GigabitEthernet0/1.50
 description AP_CONVIDADO
 encapsulation dot1Q 50
 ip address 172.18.0.3 255.255.255.128
 ip helper-address 172.18.2.68
 ipv6 address FE80::50 link-local
 ipv6 address 2001:DB8:3C4D:350::2/64
 ipv6 nd managed-config-flag
 ipv6 dhcp server STATEFUL-VLAN50
 standby 50 ip 172.18.0.1
!
interface GigabitEthernet0/1.51
 description AP_CONVIDADO
 encapsulation dot1Q 51
 ip address 172.18.0.131 255.255.255.128
 ip helper-address 172.18.2.68
 ipv6 address FE80::51 link-local
 ipv6 address 2001:DB8:3C4D:351::2/64
 ipv6 nd managed-config-flag
 ipv6 dhcp server STATEFUL-VLAN51
 standby 51 ip 172.18.0.129
!
interface GigabitEthernet0/1.99
 description GERENCIA
 encapsulation dot1Q 99
 ip address 172.18.2.131 255.255.255.240
 ipv6 address FE80::99 link-local
 ipv6 address 2001:DB8:3C4D:399::2/64
 standby 99 ip 172.18.2.129
!
interface GigabitEthernet0/2
 description TRUNK COM SW1.ADM
 no ip address
 duplex auto
 speed auto
!
interface GigabitEthernet0/2.30
 description ADM
 encapsulation dot1Q 30
 ip address 172.18.2.3 255.255.255.192
 ip helper-address 172.18.2.68
 ipv6 address FE80::30 link-local
 ipv6 address 2001:DB8:3C4D:330::2/64
 ipv6 nd managed-config-flag
 ipv6 dhcp server STATEFUL-VLAN30
 standby 30 ip 172.18.2.1
!
interface Serial0/0/0
 no ip address
 clock rate 2000000
 shutdown
!
interface Serial0/0/1
 description ONIX-ML-WAN-LINK BACKUP
 ip address 200.10.2.2 255.255.255.252
 ipv6 address FE80::1 link-local
 ipv6 address 2001:DB8:3C4D:31::2/126
!
interface Vlan1
 no ip address
 shutdown
!
router ospf 7
 router-id 1.1.1.1
 log-adjacency-changes
 passive-interface GigabitEthernet0/0.20
 passive-interface GigabitEthernet0/1.10
 passive-interface GigabitEthernet0/1.11
 passive-interface GigabitEthernet0/1.40
 passive-interface GigabitEthernet0/1.50
 passive-interface GigabitEthernet0/1.51
 passive-interface GigabitEthernet0/1.99
 passive-interface GigabitEthernet0/2.30
 network 172.18.1.0 0.0.0.127 area 3
 network 172.18.1.128 0.0.0.63 area 3
 network 172.18.1.192 0.0.0.63 area 3
 network 172.18.2.64 0.0.0.63 area 3
 network 172.18.0.0 0.0.0.127 area 3
 network 172.18.0.128 0.0.0.127 area 3
 network 172.18.2.128 0.0.0.15 area 3
 network 172.18.2.0 0.0.0.63 area 3
 network 200.10.2.0 0.0.0.3 area 0
!
ip classless
!
ip flow-export version 9
!
!
!
no cdp run
!
banner motd ^C
#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#
HOSPITAL ONIX - DEPARTAMENTO DE TI

Acesso permitido somente a pessoas autorizadas.
O acesso nao autorizado esta sujeito as penalidades da lei.

#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#
^C
!
!
!
!
!
line con 0
 password 7 08354546060A151E060A000B24223C
 login local
!
line aux 0
!
line vty 0 4
 password 7 08354546060A151E060A000B24223C
 login local
 transport input ssh
!
!
!
end
