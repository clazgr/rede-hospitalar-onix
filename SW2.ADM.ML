!
version 12.2
no service timestamps log datetime msec
no service timestamps debug datetime msec
service password-encryption
!
hostname SW2.ADM.ML
!
enable secret 5 $1$mERr$FgeFZdgS1iL/FNd8aYVM5/
!
!
!
ip ssh version 2
ip ssh authentication-retries 2
ip ssh time-out 60
no ip domain-lookup
ip domain-name onix.com
!
username onix privilege 1 password 7 08354546060A151E060A000B24223C
!
!
!
spanning-tree mode rapid-pvst
spanning-tree extend system-id
!
interface Port-channel1
 switchport mode trunk
!
interface FastEthernet0/1
 description ADM DADOS-VOIP_N2
 switchport access vlan 30
 switchport mode access
 switchport voice vlan 11
 spanning-tree portfast
 spanning-tree bpduguard enable
!
interface FastEthernet0/2
 switchport mode access
 spanning-tree portfast
 spanning-tree bpduguard enable
 shutdown
!
interface FastEthernet0/3
 switchport mode access
 spanning-tree portfast
 spanning-tree bpduguard enable
 shutdown
!
interface FastEthernet0/4
 switchport mode access
 spanning-tree portfast
 spanning-tree bpduguard enable
 shutdown
!
interface FastEthernet0/5
 switchport mode access
 spanning-tree portfast
 spanning-tree bpduguard enable
 shutdown
!
interface FastEthernet0/6
 switchport mode access
 spanning-tree portfast
 spanning-tree bpduguard enable
 shutdown
!
interface FastEthernet0/7
 switchport mode access
 spanning-tree portfast
 spanning-tree bpduguard enable
 shutdown
!
interface FastEthernet0/8
 switchport mode access
 spanning-tree portfast
 spanning-tree bpduguard enable
 shutdown
!
interface FastEthernet0/9
 switchport mode access
 spanning-tree portfast
 spanning-tree bpduguard enable
 shutdown
!
interface FastEthernet0/10
 switchport mode access
 spanning-tree portfast
 spanning-tree bpduguard enable
 shutdown
!
interface FastEthernet0/11
 switchport mode access
 spanning-tree portfast
 spanning-tree bpduguard enable
 shutdown
!
interface FastEthernet0/12
 switchport mode access
 spanning-tree portfast
 spanning-tree bpduguard enable
 shutdown
!
interface FastEthernet0/13
 switchport mode access
 spanning-tree portfast
 spanning-tree bpduguard enable
 shutdown
!
interface FastEthernet0/14
 switchport mode access
 spanning-tree portfast
 spanning-tree bpduguard enable
 shutdown
!
interface FastEthernet0/15
 switchport mode access
 spanning-tree portfast
 spanning-tree bpduguard enable
 shutdown
!
interface FastEthernet0/16
 switchport mode access
 spanning-tree portfast
 spanning-tree bpduguard enable
 shutdown
!
interface FastEthernet0/17
 description ADM AP_CORPORATIVO
 switchport access vlan 51
 switchport mode access
 spanning-tree portfast
 spanning-tree bpduguard enable
!
interface FastEthernet0/18
 switchport mode access
 spanning-tree portfast
 spanning-tree bpduguard enable
 shutdown
!
interface FastEthernet0/19
 switchport mode access
 spanning-tree portfast
 spanning-tree bpduguard enable
 shutdown
!
interface FastEthernet0/20
 switchport mode access
 spanning-tree portfast
 spanning-tree bpduguard enable
 shutdown
!
interface FastEthernet0/21
 switchport mode access
 spanning-tree portfast
 spanning-tree bpduguard enable
 shutdown
!
interface FastEthernet0/22
 switchport mode access
 spanning-tree portfast
 spanning-tree bpduguard enable
 shutdown
!
interface FastEthernet0/23
 description TRUNK ETHER CHANNEL COM SW1.ADM
 switchport mode trunk
 channel-group 1 mode desirable
!
interface FastEthernet0/24
 description TRUNK ETHER CHANNEL COM SW1.ADM
 switchport mode trunk
 channel-group 1 mode desirable
!
interface GigabitEthernet0/1
 description TRUNK COM SW2.TI
 switchport mode trunk
!
interface GigabitEthernet0/2
 description TRUNK COM SW2.MED
 switchport mode trunk
!
interface Vlan1
 no ip address
 shutdown
!
interface Vlan99
 description GERENCIA
 mac-address 0005.5e8c.9401
 ip address 172.18.2.135 255.255.255.240
!
ip default-gateway 172.18.2.129
!
banner motd 
#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#
HOSPITAL ONIX - DEPARTAMENTO DE TI

Acesso permitido somente a pessoas autorizadas.
O acesso nao autorizado esta sujeito as penalidades da lei.

#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#

!
!
!
line con 0
 password 7 08354546060A151E060A000B24223C
 login local
!
line vty 0 4
 password 7 08354546060A151E060A000B24223C
 login local
 transport input ssh
line vty 5 15
 login
!
!
!
!
end

